/**
 * STATIC EXAMPLE
 */

import * as Watchable from '../watchable.js';

let obj = { a: 1, b: 2, c: 3};

Watchable.watch(obj, 'b', (prop, old, val) => {
    console.log(prop+':', old, '=>', val);
    return val;
});
obj.b = 5;
// Expect output:
// b: 2 => 5

/**
 * get_handler()
 */
Watchable.watch(obj, 'c',
        (prop, old, val) => {
            console.log(prop+':', old, '=>', val);
            return val;
        },
        (prop, val) => {
            // optional log
            console.log('access property:', prop, ', value:', val);
            // optional breakpoint
            return val;
        },
    );
obj.c += 4
// Expect output:
// access property: c, value: 3
// c: 3 => 7

/**
 * MIXIN EXAMPLE
 */
/**
 * Adds the functions `watch()` and `unwatch()` to the prototype
 * of the function `cls()`.
 * @param {function} cls 
 */
function mixinWatchable(cls) {

    Object.defineProperties(cls.prototype, {
        watch: {
            enumerable: false,
            configurable: true,
            writable: false,
            value: function (...args) {return Watchable.watch(this, ...args)},
        },
        unwatch: {
            enumerable: false,
            configurable: true,
            writable: false,
            value: function (...args) {return Watchable.unwatch(this, ...args)},
        },
    });
    
}

function Target() {
    this.a = 1;
    this.b = 2;
    this.c = 3;
}
mixinWatchable(Target);

obj = new Target();
obj.watch('a', (prop, old, val) => {
    console.log(prop+':', old, '=>', val);
    return val;
});
obj.a = 7;
// Expect output:
// a: 1 => 7

