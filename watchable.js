/*
 * Inspired by: https://gist.github.com/eligrey/384583
 */

/**
 * @template T
 * @callback SetCallback
 * @param {string} prop
 * @param {T} old
 * @param {T} val
 * @returns {T}
 */
/**
 * @template T
 * @callback GetCallback
 * @param {string} prop
 * @param {T} val
 * @returns {T}
 */

/**
 * @param {{[key:string]:any}} obj
 * @param {string} prop 
 * @param {SetCallback<*>} set_handler - the property has already
 *      been set to the new value when this callback is called,
 *      but the callback may return a different value to reset
 *      the property.
 * @param {GetCallback<*>=} get_handler
 * @throws {Error}
 */
export function watch(obj, prop, set_handler, get_handler = (prop, x) => x) {

    // save the current value
    let value = obj[prop];

    // delete the value property
    if ( ! delete obj[prop] )   // can't watch non-configurables
        throw new Error(`Property '${prop}' is not configurable`);

    // create the new property
    Object.defineProperty(obj, prop, {
        configurable: true,
        enumerable: true,
        get: () => get_handler.call(obj, prop, value),
        set: val => {
            const before = value;
            value = val;
            const ret = set_handler.call(obj, prop, before, value);
            if (ret !== undefined) value = ret;
        },
    });

}

/**
 * @param {{[key:string]:any}} obj
 * @param {string} prop
 */
export function unwatch (obj, prop) {
    const val = obj[prop];
    delete obj[prop]; // remove accessors
    obj[prop] = val;
}
